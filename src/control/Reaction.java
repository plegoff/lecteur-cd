package control;

import ihm.*;
import javax.swing.*;
import datas.*;
import java.awt.*;
import java.awt.event.*;

public class Reaction implements ActionListener{

	private FrameLectCD frameLect;

	public Reaction (FrameLectCD f){

		this.frameLect=f;
	}
	
	public void actionPerformed (ActionEvent e){

		/*============================ REACTION BOUTON "CHARGER CD" ================================*/

		// si la source de l'objet événement "e" correspond au bouton "chargerCD"
		if(e.getSource()==frameLect.getChargerCD()){

			// si mon lecteur n'est pas chargé
			if (this.frameLect.getMonLect().estCharge()==false){
				this.frameLect.getChargerCD().setLabel("OFF");
				// on charge un CD
				this.frameLect.getMonLect().chargerUnCD();

				// affichage tps total CD + nbre plages
				String affichTTotal=this.frameLect.getMonLect().getTempsTotal();
				this.frameLect.getAffichageTpsTotal().setText(affichTTotal);
				this.frameLect.getTitreAuteur().setText("CD CHARGE - PRESS PLAY TO START");

				int affichNbPlages=this.frameLect.getMonLect().getNbrePlages();
				this.frameLect.getAffichageNbrePlages().setText(String.valueOf(affichNbPlages));

			}
			// sinon si mon lecteur est chargé
			else if(this.frameLect.getMonLect().estCharge()==true){
				this.frameLect.getChargerCD().setLabel("Charger CD");
				// on décharge le CD
				this.frameLect.getMonLect().setDecharger();

				// réinitialisation affichage tps total CD et nb plages à "0"
				this.frameLect.getAffichageTpsTotal().setText("0");
				this.frameLect.getAffichageNbrePlages().setText("0");
				this.frameLect.getNumPlage().setText("0 sec");
				this.frameLect.getDureePlage().setText("00:00:00");
				this.frameLect.getTitreAuteur().setText("AUCUN CD CHARGE");
			
				LecteurCD lecteur= this.frameLect.getMonLect();// on récupère le lecteurCD depuis la FrameLectCD

				lecteur.setDecharger();// on décharge un CD

			}
			else{
				System.out.println("erreur: on rentre dans aucune des conditions");
			}
			
		}

		/*============================ REACTION BOUTON PLAY ================================*/

		else if (e.getSource()==frameLect.getPlay()){

			this.frameLect.getMonLect().play();
			//affichage du num de plage
			int numPlage=this.frameLect.getMonLect().getIndexCourant();
			this.frameLect.getNumPlage().setText(String.valueOf(numPlage));
			//affichage du titre,auteur et duree en sec
			String titrePlage =this.frameLect.getMonLect().getPlageCourante().toString();
			this.frameLect.getTitreAuteur().setText(titrePlage);
			//affichage duree en H:M:S
			String durPlage =this.frameLect.getMonLect().getPlageCourante().getLaDuree().enTexte('H');
			this.frameLect.getDureePlage().setText(durPlage);				

		}

		/*============================ REACTION BOUTON STOP ================================*/

		else if (e.getSource()==frameLect.getStop()){

			this.frameLect.getMonLect().stop();
			int numPlage=this.frameLect.getMonLect().getIndexCourant();
			this.frameLect.getNumPlage().setText(String.valueOf(numPlage));
			//affichage du titre,auteur et duree en sec
			String titrePlage =this.frameLect.getMonLect().getPlageCourante().toString();
			this.frameLect.getTitreAuteur().setText(titrePlage);
			//affichage duree en H:M:S
			String durPlage =this.frameLect.getMonLect().getPlageCourante().getLaDuree().enTexte('H');
			this.frameLect.getDureePlage().setText(durPlage);				
		}

		/*============================ REACTION BOUTON NEXT ================================*/

		else if (e.getSource()==frameLect.getNext()){

			//condition pour empêcher de faire avancer le numero de piste au delà du nbre total de pistes
			if(this.frameLect.getMonLect().getIndexCourant()>(this.frameLect.getMonLect().getNbrePlages())-1){
				
			}
			else{
				
				this.frameLect.getMonLect().next();
				int numPlage=this.frameLect.getMonLect().getIndexCourant();
				this.frameLect.getNumPlage().setText(String.valueOf(numPlage));
				//affichage du titre,auteur et duree en sec
				String titrePlage =this.frameLect.getMonLect().getPlageCourante().toString();
				this.frameLect.getTitreAuteur().setText(titrePlage);
				//affichage duree en H:M:S
				String durPlage =this.frameLect.getMonLect().getPlageCourante().getLaDuree().enTexte('H');
				this.frameLect.getDureePlage().setText(durPlage);
			}				
		}
		
		/*============================ REACTION BOUTON PREVIOUS ================================*/

		else if (e.getSource()==frameLect.getPrevious()){

			// si on fait un previous alors qu'on est au dessus de l'indice de la plage 1
			if(this.frameLect.getMonLect().getIndexCourant()>1){

				// on descend d'un indice de plage
				this.frameLect.getMonLect().previous();
				int numPlage=this.frameLect.getMonLect().getIndexCourant();
				this.frameLect.getNumPlage().setText(String.valueOf(numPlage));
				//affichage du titre,auteur et duree en sec
				String titrePlage =this.frameLect.getMonLect().getPlageCourante().toString();
				this.frameLect.getTitreAuteur().setText(titrePlage);
				//affichage duree en H:M:S
				String durPlage =this.frameLect.getMonLect().getPlageCourante().getLaDuree().enTexte('H');
				this.frameLect.getDureePlage().setText(durPlage);
			}
			// sinon il ne se passe rien
			else{
				
			}				
		}
		else{
			System.out.println("erreur: on rentre dans aucune condition");
		}
			
	}

}