package datas;

/**
* Test de la classe Duree.java
*/
public class LecteurCDTest{
	
	public static void main(String[] args){

		//test du constructeur
		System.out.println("===================================================================================\n");
		System.out.println(" Test du constructeur LecteurCD:\n");
		testConstructeurLecteurCD();
		
		//test de l'accesseur getTempsTotal
		System.out.println("===================================================================================\n");
		System.out.println(" Test de l'accesseur getTempsTotal()\n");
		testGetTempsTotal();

		//test de l'accesseur getNbrePlages
		System.out.println("===================================================================================\n");
		System.out.println(" Test de l'accesseur getNbrePlages()\n");
		testGetNbrePlages();

		//test de l'accesseur getIndexCourant
		System.out.println("===================================================================================\n");
		System.out.println(" Test de l'accesseur getIndexCourant()\n");
		testGetIndexCourant();
		
		//test de l'accesseur getPlageCourante
		System.out.println("===================================================================================\n");
		System.out.println(" Test de l'accesseur getPlageCourante()\n");	
		testGetPlageCourante();

		//test de l'accesseur estCharge
		System.out.println("===================================================================================\n");
		System.out.println(" Test de l'accesseur estCharge()\n");
		testEstCharge();

		//test de l'accesseur getCD
		System.out.println("===================================================================================\n");
		System.out.println(" Test de l'accesseur getCD()\n");
		testGetCD();

		//test du modificateur setDecharger
		System.out.println("===================================================================================\n");
		System.out.println(" Test du modificateur setDecharger()\n");
		testSetDecharger();

		//test du modificateur chargerUnCD
		System.out.println("===================================================================================\n");
		System.out.println(" Test du modificateur chargerUnCD()\n");
		testChargerUnCD();

		//test du modificateur stop
		System.out.println("===================================================================================\n");
		System.out.println(" Test du modificateur stop()\n");
		testStop();

		//test du modificateur play
		System.out.println("===================================================================================\n");
		System.out.println(" Test du modificateur play()\n");
		testPlay();

		//test du modificateur next
		System.out.println("===================================================================================\n");
		System.out.println(" Test du modificateur next()\n");
		testNext();

		//test du modificateur previous
		System.out.println("===================================================================================\n");
		System.out.println(" Test du modificateur previous()\n");
		testPrevious();
	}

	/**
	* Test du constructeur LecteurCD
	*/
	private static void testConstructeurLecteurCD(){
		
		System.out.println(" Test cas normal: ");
		LecteurCD lectCD1 = new LecteurCD();

		//teste si le lecteurCD est bien "non chargé" à l'initialisation (test attribut estCharge) + indexPlage =0
		if(lectCD1.estCharge()==false && lectCD1.getIndexCourant() == 0){
			//on charge un CD pour tester l'attribut leCdCourant
			lectCD1.chargerUnCD();
			// test via méthodes getCD().getLeTitreCD() pour leCdCourant
			if(lectCD1.getCD().getLeTitreCD().equals("titre1")){
				System.out.println(" Test réussi\n");
			}
			else{
				System.out.println(" Echec du test\n");
			}
		}
		else{
				System.out.println(" Echec du test\n");
		}		
	}
	
		
	/*============== LES ACCESSEURS ET MODIFICATEURS ================*/

	/**
	*  Test accesseur qui renvoie le temps total du CD chargé sous forme d'une chaine de caractères.
	*/
	private static void testGetTempsTotal(){
		
		System.out.println(" Test cas normal: ");
		LecteurCD lectCD2 = new LecteurCD();
		lectCD2.chargerUnCD();

		System.out.println(" Test getTempsTotal() de CD:");
		String tpsTotalCD = lectCD2.getTempsTotal();

		if(tpsTotalCD.equals("0:11:32")){
			System.out.println(" Test réussi\n");
			System.out.println(" Temps total du CD:"+tpsTotalCD+"\n");
		}
		else{
			System.out.println(" Echec du test\n");
		}			
	}

	/**
	*  Test accesseur qui renvoie le nbre de plages du CD.
	*/
	private static void testGetNbrePlages(){
		
		System.out.println(" Test cas normal: ");
		LecteurCD lectCD3 = new LecteurCD();
		lectCD3.chargerUnCD();
			
		if(lectCD3.getNbrePlages()==3){
			System.out.println(" Test réussi\n");
		}
		else{
			System.out.println(" Echec du test\n");
		}			
	}

	/**
	*  Test accesseur getIndexCourant
	*/
	private static void testGetIndexCourant(){
		
		System.out.println(" Test cas normal: ");
		LecteurCD lectCD4 = new LecteurCD();
		//teste d'abord si l'indexPlage( l'index courant ) est à zéro à l'initialisation du lecteur
		if(lectCD4.getIndexCourant() == 0){
			
			lectCD4.chargerUnCD();
			lectCD4.play();

			if(lectCD4.getIndexCourant() == 1){
				System.out.println(" Test réussi\n");
			}
			else{
				System.out.println(" Echec du test\n");
			}
		}
		else{
				System.out.println(" Echec du test\n");
		}			
	}
		
	/**
	*  Test accesseur getPlageCourante
	*/
	private static void testGetPlageCourante(){
		
		System.out.println(" Test cas normal: ");
		LecteurCD lectCD5 = new LecteurCD();
		
		lectCD5.chargerUnCD();
		lectCD5.play();

		if(lectCD5.getPlageCourante().getLeTitre().equals("titre1")){
			System.out.println(" Test réussi\n");
		}
		else{
			System.out.println(" Echec du test\n");
		}			
	}

	/**
	*  Test accesseur estChargé
	*/
	private static void testEstCharge(){
		
		System.out.println(" Test cas normal: ");
		LecteurCD lectCD6 = new LecteurCD();
		
		//teste si le lecteurCD est bien "non chargé" à l'initialisation
		if(lectCD6.estCharge()==false){
			//on charge un CD pour tester le "true" du booléen
			lectCD6.chargerUnCD();

			if(lectCD6.estCharge()==true){
				System.out.println(" Test réussi\n");
			}
			else{
				System.out.println(" Echec du test\n");
			}
		}
		else{
				System.out.println(" Echec du test\n");
		}		
	}

	/**
	*  Test accesseur getCD
	*/
	private static void testGetCD(){
		
		System.out.println(" Test cas normal: ");
		LecteurCD lectCD7 = new LecteurCD();
		lectCD7.chargerUnCD();
		
		if(lectCD7.getCD().getLeTitreCD().equals("titre1")){
			
			System.out.println(" Test réussi\n");
		}
		else{
			System.out.println(" Echec du test\n");
		}		
	}

	/**
	*  Test modificateur setDecharger
	*/
	private static void testSetDecharger(){
		
		System.out.println(" Test cas normal: ");
		LecteurCD lectCD8 = new LecteurCD();
		lectCD8.chargerUnCD();

		if(lectCD8.estCharge()==true){

			lectCD8.setDecharger();

			if(lectCD8.estCharge()==false){
			
				System.out.println(" Test réussi\n");
			}
			else{
				System.out.println(" Echec du test\n");
			}
		}		
	}

	/**
	*  Test modificateur chargerUnCD
	*/
	private static void testChargerUnCD(){
		
		System.out.println(" Test cas normal: ");
		LecteurCD lectCD9 = new LecteurCD();
		lectCD9.chargerUnCD();

		if(lectCD9.estCharge()==true){
			System.out.println(" Test réussi\n");
		}
		else{
			System.out.println(" Echec du test\n");
		}		
	}

	/**
	*  Test modificateur stop
	*/
	private static void testStop(){
		
		System.out.println(" Test cas normal: ");
		LecteurCD lectCD10 = new LecteurCD();
		lectCD10.chargerUnCD();

		if(lectCD10.estCharge()==true){
			
			lectCD10.stop();

			if(lectCD10.getIndexCourant()==1){
				System.out.println(" Test réussi\n");
			}
			else{
				System.out.println(" Echec du test\n");
			}
		}
		else{
			System.out.println(" Echec du test\n");
		}		
	}

	/**
	*  Test modificateur play
	*/
	private static void testPlay(){
		
		System.out.println(" Test cas normal: ");
		LecteurCD lectCD11 = new LecteurCD();
		lectCD11.chargerUnCD();

		if(lectCD11.estCharge()==true){
			
			lectCD11.play();

			if(lectCD11.getIndexCourant()==1){
				System.out.println(" Test réussi\n");
			}
			else{
				System.out.println(" Echec du test\n");
			}
		}
		else{
			System.out.println(" Echec du test\n");
		}		
	}

	/**
	*  Test modificateur next
	*/
	private static void testNext(){
		
		System.out.println(" Test cas normal: ");
		LecteurCD lectCD12 = new LecteurCD();
		lectCD12.chargerUnCD();
		int indexDepart= lectCD12.getIndexCourant();
		lectCD12.next();

		if(lectCD12.getIndexCourant()==indexDepart+1){
			System.out.println(" Test réussi\n");
		}
		else{
			System.out.println(" Echec du test\n");
		}
			
	}

	/**
	*  Test modificateur previous
	*/
	private static void testPrevious(){
		
		System.out.println(" Test cas normal: ");
		LecteurCD lectCD12 = new LecteurCD();
		lectCD12.chargerUnCD();
		int indexDepart= lectCD12.getIndexCourant();
		lectCD12.previous();

		if(lectCD12.getIndexCourant()==indexDepart-1){
			System.out.println(" Test réussi\n");
		}
		else{
			System.out.println(" Echec du test\n");
		}
			
	}
}

