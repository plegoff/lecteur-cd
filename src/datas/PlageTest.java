package datas;

/**
* Test de la classe Plage.java
*/
public class PlageTest{
	
	public static void main(String[] args){

		//test du constructeur
		System.out.println("===================================================================================\n");
		System.out.println(" Test du constructeur Plage:\n");
		testConstructeurPlage();
		//test de l'accesseur getLaDuree
		System.out.println("===================================================================================\n");
		System.out.println(" Test de l'accesseur getLaDuree:\n");
		testGetLaDuree();
		//test de l'accesseur getLetitre
		System.out.println("===================================================================================\n");
		System.out.println(" Test de l'accesseur getLeTitre:\n");
		testGetLeTitre();
		//test de l'accesseur getLInterprete
		System.out.println("===================================================================================\n");
		System.out.println(" Test de l'accesseur getLInterprete:\n");
		testGetLInterprete();
		//test de l'accesseur getFicheComplete
		System.out.println("===================================================================================\n");
		System.out.println(" Test de l'accesseur getFicheComplete:\n");
		testGetFicheComplete();
		//test de l'accesseur toString
		System.out.println("===================================================================================\n");
		System.out.println(" Test de l'accesseur toString:\n");
		testToString();
		
	}

	/**
	* Test du constructeur Plage.
	*/
	private static void testConstructeurPlage(){
			
		System.out.println(" Test cas normal:");
		Duree d1 = new Duree(5, 30, 30);
		Plage p1 = new Plage (d1,"titreTest","interpreteTest");

		long tps = p1.getLaDuree().getLeTemps();
		java.lang.String titreATest = p1.getLeTitre();
		java.lang.String interpreteATest = p1.getLInterprete();
			
		if(tps==19830000 && titreATest=="titreTest" && interpreteATest=="interpreteTest"){
			System.out.println(" Test réussi\n");
		}
		else{
			System.out.println(" Echec du test\n");
		}
		
	}
		
	/**
	* Test de l'accesseur getLaDuree().
	*/
	private static void testGetLaDuree(){
			
		System.out.println(" Test cas normal:");
		Duree d1 = new Duree(5, 30, 30);
		Plage p1 = new Plage (d1,"titreTest","interpreteTest");

		long tps = p1.getLaDuree().getLeTemps();
			
		if(tps==19830000){
			System.out.println(" Test réussi\n");
		}
		else{
			System.out.println(" Echec du test\n");
		}
		
	}

	/**
	* Test de l'accesseur getLeTitre().
	*/
	private static void testGetLeTitre(){
			
		System.out.println(" Test cas normal:");
		Duree d1 = new Duree(5, 30, 30);
		Plage p1 = new Plage (d1,"titreTest","interpreteTest");

		java.lang.String titreATest = p1.getLeTitre();
			
		if(titreATest=="titreTest"){
			System.out.println(" Test réussi\n");
		}
		else{
			System.out.println(" Echec du test\n");
		}
		
	}	

	/**
	* Test de l'accesseur getLInterprete().
	*/
	private static void testGetLInterprete(){
			
		System.out.println(" Test cas normal:");
		Duree d1 = new Duree(5, 30, 30);
		Plage p1 = new Plage (d1,"titreTest","interpreteTest");

		java.lang.String interpreteATest = p1.getLInterprete();
			
		if(interpreteATest=="interpreteTest"){
			System.out.println(" Test réussi\n");
		}
		else{
			System.out.println(" Echec du test\n");
		}
	
	}

	/**
	* Test de l'accesseur getFicheComplete().
	*/
	private static void testGetFicheComplete(){
			
		System.out.println(" Test cas normal:");
		Duree d1 = new Duree(5, 30, 30);
		Plage p1 = new Plage (d1,"titreTest","interpreteTest");

		java.lang.String chaineATest =p1.getFicheComplete();
		java.lang.String chaineAttendue = "<ul><li>Description de la plage de musique :</li><li>Titre : titreTest</li><li>Interprète : interpreteTest</li><li>Durée : 5:30:30</li></ul>";
		System.out.println(chaineATest+"\n");
			
		if(chaineATest.equals(chaineAttendue)){
			System.out.println(" Test réussi\n");
		}
		else{
			System.out.println(" Echec du test\n");
		}
	
	}

	/**
	* Test de l'accesseur toString().
	*/
	private static void testToString(){
			
		System.out.println(" Test cas normal:");
		Duree d1 = new Duree(5555);
		Plage p1 = new Plage (d1,"titreTest","interpreteTest");

		java.lang.String chaineATest =p1.toString();
		java.lang.String chaineAttendue = "titreTest - interpreteTest - durée (5.555 sec)";
		System.out.println(chaineATest+"\n");

		if(chaineATest.equals(chaineAttendue)){
			System.out.println(" Test réussi\n");
		}
		else{
			System.out.println(" Echec du test\n");
		}	
	}

}
	
