package datas;

import java.util.*;

/**
* Un Cd est un ensemble de plages musicales. Le CD est caractérisé par un tritre, un interprète ( ou des inteprètes s'il s'agit d'un groupe). Lors de sa création, le CD est vierge. En le gravant, il sera rempli progressivement de morceaux (plages).
* @author Kamp, Deveaux, Sadou
*/
public class CD {

	/*============== LES ATTRIBUTS ================*/

	/**
	* Le titre du CD
	*/
	private java.lang.String leTitreCD;

	/**
	* Interprète(s) du CD
	*/
	private java.lang.String lInterpreteCD;

	/**
	* Le tableau contenant toutes les plages du CD
	*/
	private ArrayList<Plage> lesPlages;
	
	/*============== LES CONSTRUCTEURS ================*/

	/** 
	* Construction d'un CD.<br>
	* Cette construction se fait en 2 temps:
	* <ol>
	*	<li>d'abord il y a création d'un CD vierge</li>
	*	<li>ensuite il sera gravé (ajout de plages) par appel de la méthode privée "graverCD()"</li>
	* </ol>
	* @param interpreteCD - le(les) interprètes du CD
	* @param titreCD - le titre du CD
	*/
	public CD (String interpreteCD, java.lang.String titreCD){

		this.lInterpreteCD = interpreteCD;
		this.leTitreCD = titreCD;
		
		lesPlages = new ArrayList<Plage>();			
		
		this.graverCD();
	}

	/** 
	* Construction d'un CD.<br>
	* Cette construction se fait à partir d'un fichier texte qui contient toutes les informations:
	* <ul>
	*	<li>le titre et l'interprète du CD</li>
	*	<li>les différentes plages au format TitrePlage / InterprètePlage / X min. / Y sec.<br>La méthode privée "graverCD ( String leFich)" effectue la lecture du fichier texte.</li>
	* </ul>
	* @param leFich - le nom du fichier texte à lire
	*/
	public CD (String leFich){

		String leFichier = leFich;
			
	}

	/*============== LES ACCESSEURS ET MODIFICATEURS ================*/

	/**
	* Accesseur qui renvoie le nombre de plages gravées sur le CD.
	* @return le nombre total de plages
	*/
	public int getNbrePlages(){

		return this.lesPlages.size();
	}

	/**
	* Accesseur qui renvoie le titre du CD.
	* @return le titre du CD.
	*/
	public java.lang.String getLeTitreCD(){

		return this.leTitreCD;
	}

	/**
	* Accesseur qui renvoie le(les) interprètes du CD.
	* @return le(les) interprètes
	*/
	public java.lang.String getLInterpreteCD(){

		return this.lInterpreteCD;
	}

	/**
	* Accesseur qui calcule et renvoie la durée totale du CD.
	* @return la durée totale
	*/
	public Duree getDureeTotale(){

		long durTotaleCD=0;

		for(int i = 0 ; i < this.lesPlages.size(); i++){
   			durTotaleCD += this.lesPlages.get(i).getLaDuree().getLeTemps();
		}

		Duree dureeARenv = new Duree(durTotaleCD);
		return dureeARenv;
	}

	/**
	* Accesseur qui renvoie la plage n°index du CD.<br>!! La première plage du CD est à l'index = 1.
	* @param index - le numéro de la plage à renvoyer
	* @return la plage qui se trouve à l'emplacement (index -1) dans le tableau des plages 
	*/
	public Plage getUnePlage(int index){
	
		Plage plageARenv=null;

		if(index >0){
			int lIndex = (index - 1);
			plageARenv = this.lesPlages.get(lIndex);
		}
		else{
			System.out.println("index erronné");
		}

		return plageARenv;
	}

	/*============== LES METHODES ================*/

	/**
	* Méthode privée qui grave le CD. Un certain nombre de plages sont ajoutées au CD.
	* Dans cette version simplifiée, les plages sont crées et ajoutées "à la main" au CD.
	*/
	private void graverCD(){

		Duree d1 = new Duree(192000);
		Duree d2 = new Duree(200000);
		Duree d3 = new Duree(300000);
	
		Plage plage1 =new Plage(d1,"titre1","interprete1");
		Plage plage2 =new Plage(d2,"titre2","interprete2");
		Plage plage3 =new Plage(d3,"titre3","interprete3");
		
		this.lesPlages.add(plage1);
		this.lesPlages.add(plage2);
		this.lesPlages.add(plage3);

	}

	/**
	* Méthode privée qui grave le CD à partir d'un fichier. on donne un titre et un interprète au CD de
	* même qu'une liste de plages. Les informations sont lues à partir d'un fichier texte.
	* @param leFich - le nom du fichier texte à lire
	*/
	private void graverCD( String leFich){

		java.lang.String leFichier= leFich;
		
	}

}
