package datas;

/**
* Test de la classe Duree.java
*/

public class CDTest{
	
	public static void main(String[] args){

		//test du constructeur CD + graverCD + getUnePlage
		System.out.println("===================================================================================\n");
		System.out.println(" Test du constructeur CD+ graverCD + getUnePlage");
		testConstructeurCD();
		//test accesseur getNbreplages
		System.out.println("===================================================================================\n");
		System.out.println(" Test accesseur getNbrePlages");
		testGetNbrePlages();
		//test accesseur getLeTitre
		System.out.println("===================================================================================\n");
		System.out.println(" Test accesseur getLeTitre");
		testGetLeTitre();
		//test accesseur getLInterprete
		System.out.println("===================================================================================\n");
		System.out.println(" Test accesseur getLInterprete");
		testGetLInterprete();
		System.out.println("===================================================================================\n");
		System.out.println(" Test accesseur getDureeTotale");
		testGetDureeTotale();
			
	}

	/**
	* Test du constructeur CD + graverCD + getUnePlage
	*/
	private static void testConstructeurCD(){
			
		System.out.println(" Test cas normal");
				
		CD cd1 = new CD ("interprete1","titre1");
		
		String interpreteCD = cd1.getLInterpreteCD();
		String titreCD = cd1.getLeTitreCD();
				
		if(interpreteCD=="interprete1" && titreCD=="titre1"){
				System.out.println(" Test réussi\n");
		}
		else{
				System.out.println(" Echec du test\n");
		}
		
		for(int i = 1 ; i < cd1.getNbrePlages()+1; i++){
			
			long leTemps= cd1.getUnePlage(i).getLaDuree().getLeTemps();
			System.out.println(" Test arraylist plages: "+i+" : "+leTemps);
		}			
		System.out.println("\n");
	}
	
	/**
	* Test accesseur getNbrePlages
	*/
	private static void testGetNbrePlages(){
			
		System.out.println(" Test cas normal");
				
		CD cd2 = new CD ("interprete1","titre1");
		
		int nbPlagesATest = cd2.getNbrePlages();
		
		if(nbPlagesATest==3){
				System.out.println(" Test réussi\n");
		}
		else{
				System.out.println(" Echec du test\n");
		}			
		System.out.println("\n");
	}
	
	
	

	/**
	* test accesseur qui renvoie le titre du CD.
	*/
	private static void testGetLeTitre(){
			
		System.out.println(" Test cas normal");
				
		CD cd3 = new CD ("interprete1","titre1");
		
		String titre = cd3.getLeTitreCD();
		
		if(titre.equals("titre1")){
				System.out.println(" Test réussi\n");
		}
		else{
				System.out.println(" Echec du test\n");
		}			
		System.out.println("\n");
	}

	/**
	* test accesseur qui renvoie le(les) interprètes du CD.
	*/
	private static void testGetLInterprete(){
			
		System.out.println(" Test cas normal");
				
		CD cd4 = new CD ("interprete1","titre1");
		
		String interprete = cd4.getLInterpreteCD();
		
		if(interprete.equals("interprete1")){
				System.out.println(" Test réussi\n");
		}
		else{
				System.out.println(" Echec du test\n");
		}			
		System.out.println("\n");
	}

	/**
	* test accesseur qui calcule et renvoie la durée totale du CD.
	*/
	private static void testGetDureeTotale(){
			
		System.out.println(" Test cas normal");
				
		CD cd5 = new CD ("interprete1","titre1");
		
		Duree duree = cd5.getDureeTotale();
		
		long leTempsACompar = duree.getLeTemps();
		System.out.println("le temps de la duree: "+leTempsACompar+"\n");

		
		if(leTempsACompar==692000){
				System.out.println(" Test réussi\n");
		}
		else{
				System.out.println(" Echec du test\n");
		}			
		System.out.println("\n");
		
	}

}

