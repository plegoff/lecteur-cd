package datas;

/**
* Cette classe définit une plage de musique appartenant à un CD. Une plage a une durée et peut se jouer. Elle est caractérisée par un titre (titre du morceau) et un(des) interprète(s) (chanteur, musiciens, ...) du morceau de musique.
* @author Kamp, Deveaux, Sadou
*/
public class Plage {

	/*============== LES ATTRIBUTS ================*/

	/**
	* Titre du morceau de musique
	*/
	private String leTitre;

	/**
	* Interprète(s) du morceau de musique
	*/
	private String lInterprete;

	/**
	* Durée du morceau
	*/
	private Duree laDuree;
	
	/*============== LES CONSTRUCTEURS ================*/

	/** 
	* Construction d'une plage correspondant à un morceau de musique.
	* @param duree - durée de la plage (!!type Duree)
	* @param titre - titre du morceau de musique
	* @param interprete - l'interprète(les interprètes) du morceau
	*/

	public Plage(Duree duree, String titre, String interprete ){

		this.laDuree = duree;
		this.leTitre = titre;
		this.lInterprete = interprete;
	}

	/*============== LES ACCESSEURS ET MODIFICATEURS ================*/

	/**
	* Accesseur qui renvoie la durée.
	* @return la durée (!! type Duree)
	*/
	public Duree getLaDuree(){

		return this.laDuree;
	}

	/**
	* Accesseur qui renvoie le titre du morceau.
	* @return le titre
	*/
	public String getLeTitre(){

		return this.leTitre;
	}

	/**
	* Accesseur qui renvoie l'interprète(les interprètes) du morceau.
	* @return l'interprète
	*/
	public String getLInterprete(){

		return this.lInterprete;
	}

	/**
	* Accesseur qui renvoie un texte décrivant complètement la plage sous la forme suivante:<br>
	* @return le texte qui décrit la plage<br>
	* <ul>
	*	<li>Description de la plage de musique :</li>
	*	<li>Titre : ...</li>
	*	<li>Interprète : ...</li>
	*	<li>Durée : HH:MM:SS</li>
	* </ul>
	*/
	public String getFicheComplete(){
	
		String texteRenvoye ="<ul><li>Description de la plage de musique :</li><li>Titre : "+this.getLeTitre()+"</li><li>Interprète : "+this.getLInterprete()+"</li><li>Durée : "+this.getLaDuree().enTexte('H')+"</li></ul>";
		return texteRenvoye;
	}

	/**
	* Accesseur qui renvoie un résumé textuel de la plage sous la forme:<br>
	* titre - interprète - durée (SSS.MMM sec)
	* @return la ligne de texte qui décrit la plage 
	*/
	@Override
	public String toString(){
	
		String texteRenv = this.getLeTitre()+" - "+this.getLInterprete()+" - durée ("+this.getLaDuree().enTexte('S')+")";
		return texteRenv;
	}

}