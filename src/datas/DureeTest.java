package datas;

/**
* Test de la classe Duree.java
*/

public class DureeTest{
	
	public static void main(String[] args){

		//test du constructeur 1
		System.out.println("===================================================================================\n");
		System.out.println(" Test du constructeur Duree 1: Construction avec initialisation en millisecondes\n");
		testConstructeurDuree1();
		//test du constructeur 2
		System.out.println("===================================================================================\n");
		System.out.println(" Test du constructeur Duree 2 : construction par recopie d'une duree passée en paramètre.\n");
		testConstructeurDuree2();
		//test du constructeur 3
		System.out.println("===================================================================================\n");
		System.out.println(" Test du constructeur Duree 3 : construction par recopie d'une duree passée en paramètre.\n");
		testConstructeurDuree3();
		//test de l'accesseur getLeTemps()
		System.out.println("===================================================================================\n");
		System.out.println(" Test de l'accesseur getLeTemps\n");
		getLeTempsTest();
		//test de la méthode ajoute()
		System.out.println("===================================================================================\n");
		System.out.println(" Test de la méthode ajoute\n");
		testAjoute();
		//test de l'accesseur compareATest()
		System.out.println("===================================================================================\n");
		System.out.println(" Test de l'accesseur compareA()\n");
		compareATest();
		//Test de la méthode enJHMS()
		System.out.println("===================================================================================\n");
		System.out.println(" Test de la méthode enJHMS()\n");
		enJHMSTest();
		//Test de la méthode enTexte()
		System.out.println("===================================================================================\n");
		System.out.println(" Test de la méthode enTexte()\n");
		enTexteTest();

	}

		/**
		* Test du constructeur1 Duree avec Construction par recopie
		* d'une Durée passée en paramètre.
		*/
	private static void testConstructeurDuree1(){
			
		System.out.println(" Test cas normal, millisec > 0");
				
		Duree d1 = new Duree (1500);
		long tps = d1.getLeTemps();
				
		if(tps==1500){
				System.out.println(" Test réussi\n");
		}
		else{
				System.out.println(" Echec du test\n");
		}			

	}
	
		/**
		* Test du constructeur2 : construction par recopie d'une duree passée en paramètre.
		*/
	private static void testConstructeurDuree2(){
		
		System.out.println(" Test cas normal, Duree.getLeTemps()> 0");
		Duree d2 = new Duree(1500);
		Duree d3 = new Duree(d2);
		long tps = d3.getLeTemps();
			
		if(tps==1500){
			System.out.println(" Test réussi\n");
		}
		else{
			System.out.println(" Echec du test\n");
		}			

	}
			
	/**
	* Test du constructeur Duree à partir des données heures,minutes,secondes.
	*/
	private static void testConstructeurDuree3(){
		
		System.out.println(" Test cas normal, Duree.getLeTemps()> 0");
		Duree d4 = new Duree(5, 30, 30);
		long tps = d4.getLeTemps();
		
		if(tps==19830000){
			System.out.println(" Test réussi\n");
		}
		else{
			System.out.println(" Echec du test\n");
		}	

	}

	/*============== LES ACCESSEURS ET MODIFICATEURS ================*/

	/**
	* Test Accesseur qui retourne la valeur de la durée courante en millisecondes.
	*/
	private static void getLeTempsTest(){

		System.out.println(" Test cas normal, Duree.getLeTemps()=200");
		Duree d5 = new Duree (200);
		long tps = d5.getLeTemps();

		if(tps==200){
		System.out.println(" Test réussi\n");
		}
		else{
		System.out.println(" Echec du test\n");
		}

	}

	/**
	* Test Accesseur qui effectue une comparaison entre la durée
	* courante et une autre durée.
	*/
	private static void compareATest(){
			
		System.out.println(" Test cas normal, résultat attendu -1");
		Duree d6 = new Duree (200);

		if(d6.compareA(new Duree(300))==-1){
		System.out.println(" Test réussi\n");
		}
		else{
		System.out.println(" Echec du test\n");
		}
		
		System.out.println(" Test cas normal, résultat attendu 0");
		Duree d7 = new Duree (200);

		if(d7.compareA(new Duree(200))==0){
		System.out.println(" Test réussi\n");
		}
		else{
		System.out.println(" Echec du test\n");
		}

		System.out.println(" Test cas normal, résultat attendu 1");
		Duree d8 = new Duree (300);

		if(d8.compareA(new Duree(200))==1){
		System.out.println(" Test réussi\n");
		}
		else{
		System.out.println(" Echec du test\n");
		}

	}

	/**
	* Test Accesseur qui renvoit sous la forme d'une chaine de
	* caractères la durée courante.
	*/
	private static void enTexteTest(){

		System.out.println(" Test cas normal:\n");
		Duree d12 = new Duree(90061001);
		
		String texteProduit = d12.enTexte('J');
		System.out.println("texteProduit: "+texteProduit);
		String texteAttendu ="1 jours 1 h";
		System.out.println("texteAttendu: "+texteAttendu);
		if(texteAttendu.equals(texteProduit)){
			System.out.println(" Test réussi\n");
			}
			else{
			System.out.println(" Echec du test\n");
			}

		texteProduit = d12.enTexte('H');
		System.out.println("texteProduit: "+texteProduit);
		texteAttendu ="1:1:1";
		System.out.println("texteAttendu: "+texteAttendu);
		if(texteAttendu.equals(texteProduit)){
			System.out.println(" Test réussi\n");
			}
			else{
			System.out.println(" Echec du test\n");
			}

		texteProduit = d12.enTexte('S');
		System.out.println("texteProduit: "+texteProduit);
		texteAttendu ="1.1 sec";
		System.out.println("texteAttendu: "+texteAttendu);
		if(texteAttendu.equals(texteProduit)){
			System.out.println(" Test réussi\n");
			}
			else{
			System.out.println(" Echec du test\n");
			}

		texteProduit = d12.enTexte('M');
		System.out.println("texteProduit: "+texteProduit);
		texteAttendu ="1 millisec";
		System.out.println("texteAttendu: "+texteAttendu);
		if(texteAttendu.equals(texteProduit)){
			System.out.println(" Test réussi\n");
			}
			else{
			System.out.println(" Echec du test\n");
			}

		//test avec zero dans un ou plusieurs champs
		System.out.println(" Test cas normal 2:\n");
		Duree d13 = new Duree(0);
		

		texteProduit = d13.enTexte('J');
		System.out.println("texteProduit: "+texteProduit);
		texteAttendu ="0 jours 0 h";
		System.out.println("texteAttendu: "+texteAttendu);
		if(texteAttendu.equals(texteProduit)){
			System.out.println(" Test réussi\n");
			}
			else{
			System.out.println(" Echec du test\n");
			}

		texteProduit = d13.enTexte('H');
		System.out.println("texteProduit: "+texteProduit);
		texteAttendu ="0:0:0";
		System.out.println("texteAttendu: "+texteAttendu);
		if(texteAttendu.equals(texteProduit)){
			System.out.println(" Test réussi\n");
			}
			else{
			System.out.println(" Echec du test\n");
			}

		texteProduit = d13.enTexte('S');
		System.out.println("texteProduit: "+texteProduit);
		texteAttendu ="0.0 sec";
		System.out.println("texteAttendu: "+texteAttendu);
		if(texteAttendu.equals(texteProduit)){
			System.out.println(" Test réussi\n");
			}
			else{
			System.out.println(" Echec du test\n");
			}

		texteProduit = d13.enTexte('M');
		System.out.println("texteProduit: "+texteProduit);
		texteAttendu ="0 millisec";
		System.out.println("texteAttendu: "+texteAttendu);
		if(texteAttendu.equals(texteProduit)){
			System.out.println(" Test réussi\n");
			}
			else{
			System.out.println(" Echec du test\n");
			}

	}

	/**
	* Test Modificateur qui ajoute une durée à la durée courante.
	*/
	private static void testAjoute(){
		
		System.out.println(" Test cas normal, ajout de 1000 ms");
		Duree d9 = new Duree(1000);
		Duree d9b = new Duree(1000);
		d9.ajoute(d9b);
		long tps = d9.getLeTemps();
		
		System.out.println(" d9 ="+d9.getLeTemps());
		
		if(tps==2000){
			System.out.println(" Test réussi\n");
		}
		else{
			System.out.println(" Echec du test\n\n");
		}
		
		System.out.println(" Test cas d'erreur, ajout de -2000 ms");
		Duree d10 = new Duree(1000);
		d10.ajoute(new Duree(-2000));
		long tps2 = d10.getLeTemps();
		
		if(tps2==-1000){
			System.out.println("Echec du test\n");
		}
		else{
			System.out.println(" un message d'erreur s'affiche: test réussi\n\n");
		}	
	
	}

	/*============== LES METHODES ================*/

	/**
	* Test Méthode privée qui effectue un découpage de la durée courante
	* en intervalles (jours, heures, minutes, secondes, millisecondes).
	*/
	private static void enJHMSTest(){
			// methode privée à mettre en public le temps du test
			System.out.println(" Test cas normal, découpage pour 90061001 ms\nrésultat attendu = 1 partout : 1 jour, 1 heure, etc...");
			Duree d11 = new Duree(90061001);
			System.out.println("le temps de la duree: "+d11.getLeTemps());
			int tabTest[] = d11.enJHMS();
			
			System.out.println("- Jours: "+tabTest[0]+"\n- Heures: "+tabTest[1]+"\n- Minutes: "+tabTest[2]+"\n- Secondes: "+tabTest[3]+"\n- Millisecondes: "+tabTest[4]+"\n");
		
	}
}
