package datas;

/**
* La classe LecteurCD simule de manière simplifiée les fonctionnalités d'un lecteur de CD, à savoir:
* <ul>
*	<li>Le chargement d'un CD dans le lecteur</li>
*	<li>La lecture du CD depuis le début (touche PLAY)</li>
*	<li>L'arrêt de la lecture (touche STOP)</li>
*	<li>Le passage au morceau suivant (touche NEXT)</li>
*	<li>Le passage au morceau précédent (touche PREVIOUS)</li>
* </ul>
* @author Kamp, Deveaux, Sadou
*/
public class LecteurCD {

	/*============== LES ATTRIBUTS ================*/

	/**
	* Le lecteur renferme t-il un CD?
	*/
	private boolean estCharge;

	/**
	* le CD courant (qui se trouve dans le lecteur)
	*/
	private CD leCdCourant;

	/**
	* L'index de la plage courante ( 1 &lt; = index &lt; = nbrePlages ou zéro si aucun CD chargé)
	*/
	private int indexPlage;
	
	/*============== LES CONSTRUCTEURS ================*/

	/** 
	* Construction d'un lecteur de CD. A l'issue de la construction, il n'y a  aucun CD dans le lecteur
	* (leCdCourant=null). Le chargement d'un CD se fait à l'aide de la méthode "chargerUnCD()".
	*/
	public LecteurCD(){

		this.estCharge = false;
		this.leCdCourant = null;
		this.indexPlage= 0;
	}

	/*============== LES ACCESSEURS ET MODIFICATEURS ================*/

	/**
	* Accesseur qui renvoie le temps total du CD chargé sous forme d'une chaine de caractères.
	* @return le temps total du CD
	*/
	public String getTempsTotal(){
		
		Duree leTempsTotalDuree = this.leCdCourant.getDureeTotale();
		String leTempsTotalString = leTempsTotalDuree.enTexte('H');

		return leTempsTotalString;
	}

	/**
	* Accesseur qui renvoie le nombre de plages que contient le CD.
	* @return le nombre de plages (-1 si aucun CD chargé)
	*/
	public int getNbrePlages(){
		
		if (this.estCharge()==false){
			return -1;
		}
		else{
			int nbrePlages= this.getCD().getNbrePlages();
			return nbrePlages;
		}
	}
	
	/**
	* Accesseur qui renvoie l'index de la la plage du CD en cours de lecture. Cet index est compris entre 1 et nbrePlages.
	* @return l'index de la plage courante (zéro si aucun CD chargé)
	*/
	public int getIndexCourant(){
		
		if (this.estCharge()==false){
			return 0;
		}
		else{
			int indexCourant= this.indexPlage;
			return indexCourant;
		}
	}
	
	/**
	* Accesseur qui renvoie la plage en cours de lecture.
	* @return la plage courante (null si aucun CD chargé)
	*/
	public Plage getPlageCourante(){
		
		if (this.estCharge()==false){
			return null;
		}
		else{
			int indexP= this.getIndexCourant();
			Plage laPlage= this.getCD().getUnePlage(indexP);
			return laPlage;
		}
	}

	/**
	* Accesseur qui renvoie vrai si le lecteur contient un CD.
	* @return vrai si il y a un CD dans le lecteur
	*/
	public boolean estCharge(){
		
		if (this.estCharge==false){
			return false;
		}
		else{
			return true;
		}
	}

	/**
	* Accesseur qui renvoie le CD chargé dans le lecteur ou null si aucun CD chargé.
	* @return le CD courant ou null si aucun CD dans le lecteur
	*/
	public CD getCD(){
		
		if (this.estCharge()==false){
			return null;
		}
		else{
			return this.leCdCourant;
		}
	}

	/**
	* Modificateur qui force le lecteur à se décharger du CD qu'il contient. Le booléen estCharge
	* devient faux et le CdCourant devient null.
	*/
	public void setDecharger(){
		
		this.estCharge = false;
		this.leCdCourant = null;
		
		// n'est pas demandé précisément dans le sujet. je l'ai mis par précaution
		this.indexPlage=0;
	}
	
	/**
	* Modificateur dont le rôle est de charger un CD dans le lecteur; pour simplifier, il s'agira toujours du même
	* CD pour cette version de la méthode. La méthode doit construire le CD et mettre le booléen à vrai.
	*/
	public void chargerUnCD(){
		
		this.estCharge = true;
		this.leCdCourant = new CD ("interprete1","titre1");
		
	}
	
	/**
	* Modificateur qui simule la touche STOP. Cela a pour conséquence de simplement remettre
	* l'index des plages sur 1. (Et éventuellement de démarrer le timer de lecture de la plage suivante)
	* Il ne se passe rien si aucun CD chargé.
	*/
	public void stop(){
		
		if (this.estCharge()==false){
			
		}
		else{
			this.indexPlage=1;
		}
	}
	
	/**
	* Modificateur qui simule la touche PLAY. Cela a pour conséquence de simplement remettre
	* l'index des plages sur 1. (Et éventuellement de démarrer le timer de lecture de la plage suivante)
	* Il ne se passe rien si aucun CD chargé.
	*/
	public void play(){
		
		if (this.estCharge()==false){
			
		}
		else{
			this.indexPlage=1;
		}
	}
	
	/**
	* Modificateur qui simule la touche NEXT. Cela a pour conséquence de simplement incrémenter
	* l'index des plages. (Et éventuellement de démarrer le timer de lecture de la plage suivante)
	* Il ne se passe rien si aucun CD chargé.
	*/
	public void next(){
		
		if (this.estCharge()==false){
			
		}
		else{
			this.indexPlage+=1;
		}
	}

	/**
	* Modificateur qui simule la touche PREVIOUS. Cela a pour conséquence de simplement décrémenter
	* l'index des plages. (Et éventuellement de démarrer le timer de lecture de la plage suivante)
	* Il ne se passe rien si aucun CD chargé.
	*/
	public void previous(){
		
		if (this.estCharge()==false){
			
		}
		else{
			this.indexPlage-=1;
		}
	}

}
