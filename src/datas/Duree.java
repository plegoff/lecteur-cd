package datas;

/**
* Cette classe définit une durée temporelle. Elle permet la manipulation
* d'intervalles de temps. Une durée s'exprime en millisecondes.
* @author Kamp, Deveaux, Sadou
*/
public class Duree {

	/**
	* la durée s'exprime en millisecondes
	*/
	private long leTemps;
	
	/*============== LES CONSTRUCTEURS ================*/

	/** 
	* Constructeur1: Constructeur avec initialisation en millisecondes.
	* @param millisec - la durée exprimée en millisecondes.
	*/

	public Duree(long millisec){

		if (millisec > 0){
			
			this.leTemps=millisec;
		}
		else{
			
			System.out.println("veuillez entrer un nombre strictement positif svp");
		}
	}

	/**
	* Constructeur2: Construction par recopie d'une Durée passée en paramètre.
	* @param autreDuree - la durée à copier
	*/
	public Duree(Duree autreDuree){
		
		if (autreDuree.getLeTemps() > 0){
			
			this.leTemps=autreDuree.getLeTemps();
		
		}
		else{
			System.out.println("veuillez entrer nombre strictement positif svp");
		}
	}
	
	/**
	* Constructeur3: Constructeur à partir des données heures,minutes,secondes.
	* @param heures - nbre d'heures<br>
	* @param minutes - nbre de minutes<br>
	* @param secondes - nbre de secondes
	*/
	public Duree(int heures, int minutes, int secondes){
			
		this.leTemps = (heures*3600000)+(minutes*60000)+(secondes*1000);
			
		if(this.leTemps <0){
			
			System.out.println("veuillez entrer nombre entier strictement positif svp");
		}	
	}

	/*============== LES ACCESSEURS ET MODIFICATEURS ================*/

	/**
	* Accesseur qui retourne la valeur de la durée courante en millisecondes.
	* @return la durée en millisecondes
	*/
	public long getLeTemps(){

		return this.leTemps;
	}

	/**
	* Accesseur qui effectue une comparaison entre la durée courante et une autre durée.
	* @param autreDuree - durée à comparer à la durée courante
	* @return un entier qui prend les valeurs suivantes<br>
	* <ul>
	*	<li>-1 : si la durée courante est + petite que autreDuree</li>
	*	<li>0 : si la durée courante est égale à autreDuree</li>
	*	<li>1 : si la durée courante est + grande que autreDuree</li>
	* </ul>
	*/
	public int compareA (Duree autreDuree){
	
		Duree lAutreDuree = autreDuree;
		long dureeCourante = this.getLeTemps();
		long dureeAComparer = lAutreDuree.getLeTemps();

		if(dureeCourante < dureeAComparer){

				return -1;
		}
      	else if(dureeCourante == dureeAComparer){

				return 0;
		}
		else{
				return 1;
		}
			
	}

	/**
	* Accesseur qui renvoit sous la forme d'une chaîne de caractères la durée courante.
	* @param mode - décide de la forme donnée à la chaîne de caractères<br>
	* La forme de la chaîne de caractères dépend du "mode" (caractère passé en paramètre) choisi:<br>
	* <ul>
	*	<li>si mode =='J'=&gt; chaîne de caractères de la forme "JJJ jours HH h"</li>
	*	<li>si mode =='H'=&gt; chaîne de caractères de la forme "HHH:MM:SS"</li>
	*	<li>si mode =='S'=&gt; chaîne de caractères de la forme "SSS.MMM sec"</li>
	*	<li>si mode =='M'=&gt; chaîne de caractères de la forme "MMMMMM millisec"</li>
	* </ul>
	* @return la durée sous la forme d'une chaîne de caractères<br><br>
	* La méthode utilise la méthode privée en JHMS()pour extraire dans un tableau d'entiers séparément le nombre de jours, le nombre d'heures, le nombre de minutes, le nombre de secondes et le nombre de millisecondes que contient la durée courante (leTemps).
	*/
	public String enTexte( char mode){

		char leMode = mode;
		String leStringRenvoye = null;
		int tab[] = new int[5];
   		tab=this.enJHMS();

		switch (leMode) {

	  		case 'J':	
	   			leStringRenvoye= tab[0]+" jours "+tab[1]+" h";	
	    	break;
	    	
	    	case 'H':
	   			leStringRenvoye= tab[1]+":"+tab[2]+":"+tab[3];	
	    	break;

	    	case 'S':
	   			leStringRenvoye= tab[3]+"."+tab[4]+" sec";	
	    	break;

	    	case 'M':
	   			leStringRenvoye= tab[4]+" millisec";	
	    	break;
	    	
	  		default:
	    		leStringRenvoye = "erreur d'entrée";
   			
		}
		return leStringRenvoye;
	}

	/**
	* Modificateur qui ajoute une durée à la durée courante.
	* @param autreDuree - durée à rajouter
	*/
	public void ajoute(Duree autreDuree){

		if (autreDuree.getLeTemps() > 0){
			
			this.leTemps += autreDuree.getLeTemps();
		
		}
		else{
			System.out.println("veuillez entrer nombre strictement positif svp");
		}
	}

	/*============== LES METHODES ================*/

	/**
	* Méthode privée qui effectue un découpage de la durée courante
	* en intervalles (jours, heures, minutes, secondes, millisecondes).
	* La durée courante (leTemps) est analysée pour fabriquer un tableau d'entiers
	* (taille 5) dont chaque élément a la signification suivante:<br>
	* <ul>
	*	<li>ret[0] contient le nbre de jours</li>
	*	<li>ret[1] contient le nbre d'heures(&lt;24h)</li>
	*	<li>ret[2] contient le nbre de minutes (&lt;60min)</li>
	*	<li>ret[3] contient le nbre de secondes (&lt;60sec)</li>
	*	<li>ret[4] contient le nbre de millisecondes (&lt;1000millisec)</li>
	* </ul>
	* @return un tableau d'entiers
	*/
	public int[] enJHMS(){

		int reste = 0;
		int unJour = 86400000;
		int uneHeure = 3600000;
		int uneMinute = 60000;
		int uneSeconde = 1000;
		int leTemps = (int)this.getLeTemps();
		int ret[] = new int[5];

		// remplit indice 0 du tableau avec le nombre de jours
		ret[0] = leTemps/unJour;
		reste = leTemps%unJour;

		// remplit indice 1 du tableau avec le nombre d'heures'
		ret[1] = reste/uneHeure;
		reste = reste%uneHeure;

		// remplit indice 2 du tableau avec le nombre de minutes
		ret[2] = reste/uneMinute;
		reste = reste%uneMinute;

		// remplit indice 3 du tableau avec le nombre de secondes
		ret[3] = reste/uneSeconde;
		reste = reste%uneSeconde;

		// remplit indice 3 du tableau avec le nombre de millisecondes
		ret[4] = reste;

		return ret;
		
	}

}
