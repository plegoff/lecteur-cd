package ihm;

import control.*;
import javax.swing.*;
import datas.*;
import java.awt.*;
import java.awt.event.*;



public class FrameLectCD extends JFrame{

	//Attributs = les widgets
	private JButton chargerCD;
	private JTextField espaceVide;
	private JLabel tempsTotal;
	private JTextField affichageTpsTotal;
	private JLabel nbrePlages;
	private JTextField affichageNbrePlages;
	private JTextField numPlage;
	private JTextField titreAuteur;
	private JTextField dureePlage;
	private JButton stop;
	private JButton play;
	private JButton next;
	private JButton previous;

	// sert à repérer ON ou OFF pour changer le label du bouton ChargerCD
	private String flag1="OFF";
	//....
	private LecteurCD monLect;

	/*============== CONSTRUCTEUR ================*/

	public FrameLectCD(){

		super();
		this.monLect=new LecteurCD();
		this.miseEnPlaceDecor();
		this.attacherReactions();

		this.setTitle("LecteurCD"); //On donne un titre à l'application
		this.setSize(800,400); //On donne une taille à notre fenêtre
		this.setLocationRelativeTo(null); //On centre la fenêtre sur l'écran
		//this.setResizable(false); //On interdit la redimensionnement de la fenêtre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //On dit à l'application de se fermer lors du clic sur la croix

	}

	private void miseEnPlaceDecor(){

		this.chargerCD=new JButton("ChargerCD");
		this.espaceVide=new JTextField("");
		this.espaceVide.setEditable( false );
		this.tempsTotal=new JLabel("Temps total");
		this.affichageTpsTotal=new JTextField("0");
		this.affichageTpsTotal.setEditable( false );
		this.nbrePlages=new JLabel("Nombre de plages");
		this.affichageNbrePlages=new JTextField("0");
		this.affichageNbrePlages.setEditable( false );
		this.numPlage=new JTextField();
		this.numPlage.setPreferredSize (new Dimension(100,50));
		this.numPlage.setEditable( false );
		this.titreAuteur=new JTextField("AUCUN CD CHARGE");
		this.titreAuteur.setEditable( false );
		this.dureePlage=new JTextField();
		this.dureePlage.setPreferredSize (new Dimension(100,50));
		this.dureePlage.setEditable( false );
		this.stop=new JButton("STOP");
		this.play=new JButton("PLAY");
		this.next=new JButton("NEXT");
		this.previous=new JButton("PREVIOUS");

		JPanel composant1 = new JPanel();//composant du haut (pleine largeur) qui comprend 3 lignes et 2 colonnes
		composant1.setLayout(new GridLayout(3,2));
		composant1.add(chargerCD);// JButton charger CD-on
		composant1.add(espaceVide);// JPanel espace vide
		composant1.add(tempsTotal);// JLabel Temps total
		composant1.add(affichageTpsTotal);// JTextField affichage tps total
		composant1.add(nbrePlages);// JLabel nbre de plages
		composant1.add(affichageNbrePlages);// JTextField affichage nbre de plages

		JPanel composant2 = new JPanel();//composant du milieu (pleine largeur) qui comprend 3 zones: WEST, CENTER et EAST
		composant2.setLayout(new BorderLayout());
		composant2.add(numPlage, BorderLayout.WEST);//JTextField WEST = numero plage
		composant2.add(titreAuteur, BorderLayout.CENTER);//JTextField CENTER = infos plage
		composant2.add(dureePlage, BorderLayout.EAST);//JTextField EAST = duree plage

		JPanel composant3 = new JPanel();//composant du bas (pleine largeur) qui comprend 1 ligne et 4 colonnes
		composant3.setLayout(new GridLayout(1,4));
		composant3.add(stop);// JButton STOP
		composant3.add(play);// JButton PLAY
		composant3.add(next);// JButton NEXT
		composant3.add(previous);// JButton PREVIOUS

		this.setLayout(new GridLayout(3,1)); //Crée un layout de 3 lignes et 1 colonne
		this.add(composant1);
		this.add(composant2);
		this.add(composant3);

	}

	private void attacherReactions(){

		this.chargerCD.addActionListener(new Reaction(this));
		this.play.addActionListener(new Reaction(this));
		this.numPlage.addActionListener(new Reaction(this));
		this.dureePlage.addActionListener(new Reaction(this));
		this.titreAuteur.addActionListener(new Reaction(this));
		this.play.addActionListener(new Reaction(this));
		this.stop.addActionListener(new Reaction(this));
		this.next.addActionListener(new Reaction(this));
		this.previous.addActionListener(new Reaction(this));
	}

	/*======================== LES ACCESSEURS =============================*/

	public LecteurCD getMonLect(){
		return this.monLect;
	}

	public String getFlag1(){
		return this.flag1;
	}

	public void setFlag1(String txt){
		this.flag1=txt;
	}

	public JButton getChargerCD(){
		return this.chargerCD;
	}

	public void setTextChargerCD(String txt){
		this.chargerCD.setText(txt);
	}

	public JTextField getEspaceVide(){
		return this.espaceVide;
	}

	public JLabel getTempsTotal(){
		return this.tempsTotal;
	}

	public JTextField getAffichageTpsTotal(){
		return this.affichageTpsTotal;
	}

	public void setAffichageTpsTotal(String txt){
		String leTxt=txt;
		this.affichageTpsTotal.setText(leTxt);
	}

	public JLabel getNbrePlages(){
		return this.nbrePlages;
	}

	public JTextField getAffichageNbrePlages(){
		return this.affichageNbrePlages;
	}

	public void setAffichageNbrePlages(String txt){
		this.affichageNbrePlages.setText("texteTest2");
	}

	public JTextField getNumPlage(){
		return this.numPlage;
	}

	public JTextField getTitreAuteur(){
		return this.titreAuteur;
	}

	public JTextField getDureePlage(){
		return this.dureePlage;
	}

	public JButton getStop(){
		return this.stop;
	}

	public JButton getPlay(){
		return this.play;
	}

	public JButton getNext(){
		return this.next;
	}

	public JButton getPrevious(){
		return this.previous;
	}

	//lanceur de l'appli
	public static void main(String[] args){

				FrameLectCD appli = new FrameLectCD();
				appli.setVisible(true);
	}



}